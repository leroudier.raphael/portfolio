# README

Projet de creation d'une page web " *Portfolio* " et de sa mise en page en htlm & css avec
du responsive, pour le site simplonlyon.fr dans lequel figures :

- Une petite présentation
- La liste de mes projets
- mon contact mail
- Le lien vers mon gitlab, git hub, likedin
- Mon cv
- Animations JS pour le slide images du header ansi que pour le swipe des des jeux

Le site est responsive et valide W3C.



## Users Stories

1. Je suis un élève de Simplon voulant créer un Portfolio je me rend sur le site de Simplon pour voir le portfolio des années précédantes.
2. Je suis un employeur et souhaite me renseigner sur les compétences des apprennants je me rend sur le site et examine les differents portfolios.
3. je suis le créateur du portfolio je veut vérifier les fonctionalités de mon site pour analiser la validité de ce dernier.



## Maquettes Fonctionnels

### Version First mobile:

![version-mobile](images/version-mobile.png)



### Version Desktop:

![maquette-desktop](images/maquette-desktop.png)
